﻿using UnityEngine;

public class ViewMVC : MonoBehaviour
{
    internal ModelMVC model;

    /// <summary>
    /// Updates data with passed data and then updates view
    /// </summary>
    public virtual void Setup(ModelMVC model)
    {
        UpdateData(model);
        UpdateVisuals();
    }

    /// <summary>
    /// Only updates data, does not update visuals
    /// </summary>
    public virtual void UpdateData(ModelMVC model)
    {
        this.model = model;
    }

    /// <summary>
    /// Updates view visuals - visible UI elements, 3d models etc.
    /// </summary>
    public virtual void UpdateVisuals()
    {

    }
}

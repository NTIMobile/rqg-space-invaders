﻿using UnityEngine;

public class PlayerController : ControllerMVC<PlayerController>
{
    #region data fields and properties
    [Header("Basic config")]
    [SerializeField] private PlayerConfig playerConfig;
    [SerializeField] private Transform playerSpawn;

    public int CurrentLifes
    {
        get
        {
            return playerShipView != null ? playerShipView.ShipModel.ShipLifes : 0;
        }
    }
    private PlayerShipView playerShipView;
    #endregion

    /// <summary>
    /// On awake add action to touch - player ship moving
    /// </summary>
    private void Awake()
    {
        if (TouchController.Instance != null)
            TouchController.Instance.AddOnTouchHoldAction(MoveShipAndShoot);
    }

    /// <summary>
    /// When playing game, move ship and shoot
    /// </summary>
    private void Update()
    {
        if (playerShipView != null && playerShipView.IsAlive() && AppStateController.Instance.IsGamePlaying())
            playerShipView.MoveToTargetPosition();
    }

    /// <summary>
    /// Setup this controller - spawn NPCs if not spawned, set initial values etc.
    /// </summary>
    public void Setup()
    {
        if (playerShipView == null)
        {
            GameObject foundPrefab = AssetController.Instance?.LoadedViewAssets.Find(match => match.GetComponent<PlayerShipView>() != null);
            if (foundPrefab != null)
            {
                playerShipView = Instantiate(foundPrefab, playerSpawn != null ? playerSpawn : transform).GetComponent<PlayerShipView>();
            }
        }
        if (playerShipView != null)
        {
            ScoreController.Instance.ResetScore();
            playerShipView.SetState(ShipViewState.Fine);
            playerShipView.Setup(playerConfig.ShipModel, ShowReslut);
        }
    }

    /// <summary>
    /// Requests app state controller to show result state
    /// </summary>
    private void ShowReslut()
    {
        _ = AppStateController.Instance.SetState(new GameResultState());
    }

    /// <summary>
    /// Moves ship and shoots
    /// </summary>
    /// <param name="pos"></param>
    private void MoveShipAndShoot(float pos)
    {
        if (playerShipView != null)
        {
            playerShipView.MoveShip(pos);
            playerShipView.ShootBullet();
        }
    }

    /// <summary>
    /// Deactivates player ship
    /// </summary>
    internal void Deactivate()
    {
        if (playerShipView != null)
        {
            playerShipView.SetState(ShipViewState.Destroyed);
        }
    }
}

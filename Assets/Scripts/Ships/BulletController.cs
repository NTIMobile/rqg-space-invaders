﻿using System.Collections.Generic;
using UnityEngine;

public class BulletController : ControllerMVC<BulletController>
{
    #region data fields and properties
    [SerializeField] private string playerBulletLayerName= "PlayerBullet", enemyBulletLayerName= "EnemyBullet";

    public string PlayerBulletLayerName
    {
        get
        {
            return playerBulletLayerName;
        }
    }
    public string EnemyBulletLayerName
    {
        get
        {
            return enemyBulletLayerName;
        }
    }

    private List<BulletView> allBullets;
    #endregion

    /// <summary>
    /// Shooting bullet method. At first it tries to reuse any from scene. If not found, then creates one
    /// </summary>
    /// <param name="fromTransform"></param>
    /// <param name="friendly"></param>
    public void ShootBullet(Transform fromTransform, bool friendly = true)
    {
        BulletView newBullet = null;
        if (allBullets != null)
        {
            newBullet = allBullets.Find(match => match.Reuse(fromTransform, friendly));
        }
        else
        {
            allBullets = new List<BulletView>();
        }
        if (newBullet == null)
        {
            GameObject foundPrefab = AssetController.Instance?.LoadedViewAssets.Find(match => match.GetComponent<BulletView>() != null);
            if (foundPrefab != null)
            {
                newBullet = Instantiate(foundPrefab).GetComponent<BulletView>();
                newBullet.Setup(fromTransform, friendly);
                allBullets.Add(newBullet);
            }
        }
    }
}

﻿using UnityEngine;

[CreateAssetMenu(menuName = "Config/NPCs", fileName = "NPCsConfig")]
public class NPCsConfig : ScriptableObject
{
    #region data fields and properties
    [SerializeField] private int spawnNPCsAtEveryWave, gunThresholdMiliseconds = 100, shootingThresholdMiliseconds;
    [SerializeField] private float shipEnergy, shipMovementSpeed;

    public int SpawnNPCsAtEveryWave
    {
        get
        {
            return Mathf.Clamp(spawnNPCsAtEveryWave, 5, 30);
        }
    }

    public int ShootingThresholdMiliseconds
    {
        get
        {
            return Mathf.Clamp(shootingThresholdMiliseconds, 100, 2000);
        }
    }

    public ShipModel ShipModel
    {
        get
        {
            ShipModel shipModel = new ShipModel(1,
                Mathf.Clamp(shipEnergy, 10, 1000),
                Mathf.Clamp(shipMovementSpeed, 1, 50),
                false,
                Mathf.Clamp(gunThresholdMiliseconds, 20, 2000));
            return shipModel;
        }
    }
    #endregion
}
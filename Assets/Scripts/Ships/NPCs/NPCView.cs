﻿using UnityEngine;

public class NPCView : ShipView
{
    #region data fields and properties
    private GameObject currentPrefab;
    #endregion

    /// <summary>
    /// Method for "shuffling" NPC model
    /// </summary>
    public void ShuffleModel()
    {
        if (currentPrefab != null)
            Destroy(currentPrefab);

        GameObject foundPrefab = AssetController.Instance.GetRandomNPCMesh();
        if (foundPrefab != null)
        {
            currentPrefab = Instantiate(foundPrefab, transform);
        }
    }
}

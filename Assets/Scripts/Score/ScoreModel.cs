﻿using System;

[System.Serializable]
public class ScoreModel
{
    #region data fields and properties
    public int score;
    public DateTime achievedAt;
    #endregion

    public ScoreModel(int score)
    {
        this.score = score;
        achievedAt = DateTime.Now;
    }
}

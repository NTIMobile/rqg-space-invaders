﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : ControllerMVC<ScoreController>
{
    #region data fields and properties
    public int CurrentScore
    {
        get;
        private set;
    } = 0;
    public int LastScore
    {
        get;
        private set;
    } = 0;

    private const string fileName = "score";
    private List<ScoreModel> savedScores;
    #endregion

    #region current score operations
    /// <summary>
    /// Resets score
    /// </summary>
    public void ResetScore()
    {
        CurrentScore = 0;
    }

    /// <summary>
    /// Tries to save score, if possible
    /// </summary>
    public void SaveScore()
    {
        CompareWithBestScoresAndSave();
    }

    /// <summary>
    /// Add points to current score
    /// </summary>
    /// <param name="toAdd"></param>
    public void AddToCurrentScore(int toAdd)
    {
        if (toAdd > 0 && AppStateController.Instance.IsGamePlaying())
        {
            CurrentScore += toAdd;
            LastScore = CurrentScore;
        }
    }
    #endregion

    #region saved scores operations
    /// <summary>
    /// Returns scores, if not loaded - loads
    /// </summary>
    /// <returns></returns>
    public List<ScoreModel> GetScores()
    {
        return savedScores == null ? LoadScores() : savedScores;
    }

    /// <summary>
    /// Tries to load scores
    /// </summary>
    /// <returns>List of scores</returns>
    private List<ScoreModel> LoadScores()
    {
        if (savedScores == null)
        {
            savedScores = new List<ScoreModel>();
            object saveObject = SaveLoadManager.Load(fileName);
            if (saveObject != null)
            {
                try
                {
                    savedScores = saveObject as List<ScoreModel>;
                }
                catch (Exception ex)
                {
                    Debug.LogError($"Cannot parse object to List<int>(). Exception:{ex}");
                }
            }
            savedScores.Sort((a, b) => (b.score.CompareTo(a.score)));
        }
        return savedScores;
    }

    /// <summary>
    /// Compares with current scores, puts on list if possible
    /// </summary>
    private void CompareWithBestScoresAndSave()
    {
        if (savedScores == null)
        {
            LoadScores();
        }
        if (savedScores != null)
        {
            ScoreModel newScore = new ScoreModel(CurrentScore);
            savedScores.Add(newScore);
            savedScores.Sort((a, b) => (b.score.CompareTo(a.score)));
            while (savedScores.Count > 10)
            {
                savedScores.RemoveAt(10);
            }
            SaveScores();
        }
    }

    /// <summary>
    /// Saves currently stored in memory scores
    /// </summary>
    private void SaveScores()
    {
        SaveLoadManager.Save(fileName, savedScores);
    }
    #endregion
}

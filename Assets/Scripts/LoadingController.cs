﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingController : ControllerMVC<LoadingController>
{
    #region data fields and properties
    private AsyncOperation currentLoadingOperation;
    #endregion

    /// <summary>
    /// Loades selected scene and shows loading scrren
    /// </summary>
    /// <param name="selectedScene">which scene to load</param>
    public async Task LoadSceneAsync(SceneName selectedScene, Action additionalActionAfterLoaded = null)
    {
        if (currentLoadingOperation != null)
            return;
        currentLoadingOperation = SceneManager.LoadSceneAsync(selectedScene.ToString(), LoadSceneMode.Additive);
        while (currentLoadingOperation.progress < 0.9f)
        {
            await Task.Delay(100);
            UIController.Instance?.SetLoadingProgress(currentLoadingOperation.progress*100);
        }
        UIController.Instance?.SetLoadingProgress(100);
        await Task.Delay(100);
        currentLoadingOperation = null;
        if (additionalActionAfterLoaded != null)
            additionalActionAfterLoaded.Invoke();
    }
}

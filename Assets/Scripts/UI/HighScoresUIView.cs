﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HighScoresUIView : UIView
{
    #region data fields and properties
    [SerializeField] private TextMeshProUGUI scoresTMP;
    #endregion

    /// <summary>
    /// Refreshes this UI with new data
    /// </summary>
    internal override void RefreshUI()
    {
        base.RefreshUI();
        RefreshScoresText(ScoreController.Instance.GetScores());
    }

    /// <summary>
    /// Sets score text with passed data, if none indicates that also
    /// </summary>
    /// <param name="scores"></param>
    public void RefreshScoresText(List<ScoreModel> scores)
    {
        if (scoresTMP == null)
            return;
        if (scores == null || scores.Count == 0)
        {
            scoresTMP?.SetText($"No saved scores!");
        }
        else
        {
            scoresTMP.SetText($"Best scores:");
            for (int i = 0; i < scores.Count; i++)
            {
                scoresTMP.text += $"\n{i + 1}. place: {scores[i].score} pts., achieved at {scores[i].achievedAt.ToShortDateString()}, {scores[i].achievedAt.ToShortTimeString()}";
            }
        }
    }

    /// <summary>
    /// Returns to main menu
    /// </summary>
    public void BackToMenu()
    {
        _ = AppStateController.Instance.SetState(new MenuState());
    }
}

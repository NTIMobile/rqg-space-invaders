﻿public enum UIType
{
    LoadingScreen,
    MainMenu,
    Gameplay,
    Results,
    HighScores
}
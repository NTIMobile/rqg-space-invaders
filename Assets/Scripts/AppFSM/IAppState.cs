﻿using System;
using System.Threading.Tasks;

public interface IAppState
{
    Task<IAppState> DoState();
}

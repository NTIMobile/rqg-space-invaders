﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveLoadManager : ManagerMVC<SaveLoadManager>
{
    #region data fields and properties
    private static string mainPath = $"{Application.persistentDataPath}/";
    #endregion

    /// <summary>
    /// Load object from persistent data by given filename
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns>Data Object which should be casted to target data class</returns>
    public static object Load(string fileName)
    {
        string path = mainPath+$"{fileName}.data";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            stream.Position = 0;
            object Data = formatter.Deserialize(stream) as object;
            stream.Close();
            return Data;
        }
        else
        {
            Debug.LogWarning("Cannot load game. Save file not found in " + path);
            return null;
        }
    }

    /// <summary>
    /// Save object to persistent data under given filename
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="data"></param>
    public static void Save(string fileName, object data)
    {
        string path = mainPath + $"{fileName}.data";
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Create);
        stream.Position = 0;
        object Data = data;
        formatter.Serialize(stream, Data);
        stream.Close();
    }
}